package com.swiftit.himsApp.service;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.swiftit.himsApp.config.Response;


public interface StorageService {

	void init();

//	Response storeFile(MultipartFile file);

	Response storeFile(MultipartFile file, String fileName, String subDir);

	Stream<Path> loadAll();

	Path load(String filename);

	void deleteAll();
	
	Resource loadFileAsResource(String filename);

	Resource loadFileAsResourceWithSubDir(String fileName, String subDir);


}
