package com.swiftit.himsApp.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.swiftit.himsApp.config.AppException;
import com.swiftit.himsApp.config.Response;
import com.swiftit.himsApp.config.StorageProperties;


@Service
public class FileStorageService implements StorageService {
	
	private final Path fileStorageLocation;

    @Autowired
    public FileStorageService(StorageProperties storageProperties) {
    	
        this.fileStorageLocation = Paths.get(storageProperties.getUploadDir()).toAbsolutePath().normalize();
//        this.fileStorageLocation = Paths.get(storageProperties.getUploadDir());

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw AppException.fileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    @Override
    public Response storeFile(MultipartFile file, String fileName, String subDir) {
    	
    	String originalFileName = file.getOriginalFilename();
    	String fileDirectory = "";
    	String mFileName = "";
    	
    	try {
            if(!isEmpty(fileName)) {
            	String fileExtention = "";
                int index = originalFileName.lastIndexOf('.');
                if(index > 0) {
                	fileExtention = originalFileName.substring(index + 1);
                }
                // Normalize file name
                mFileName = StringUtils.cleanPath(fileName+"."+fileExtention);
            } else {
            	// Normalize file name
                mFileName = StringUtils.cleanPath(originalFileName);
            }
            
    		// Check if the file's name contains invalid characters
    		if(mFileName.contains("..") || mFileName.contains("/")) {
//    			throw AppException.fileStorageException("Sorry! Filename contains invalid path sequence " + mFileName);
    			return new Response(false, "Filename Contains Invalid Character!");
    		} 
    		if(!isEmpty(subDir)) {
    			Files.createDirectories(Paths.get(this.fileStorageLocation+"/"+subDir).toAbsolutePath().normalize());
    			fileDirectory = (subDir +"/"+ mFileName).toLowerCase();
    		} else {
    			fileDirectory = mFileName.toLowerCase();
    		}
    		// Copy file to the target location (Replacing existing file with the same name)
    		Path targetLocation = this.fileStorageLocation.resolve(fileDirectory);
    		
    		Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
    		
    		return new Response(true, "Successfully File Stored", mFileName, fileDirectory, file.getContentType(), file.getSize());
    	} catch (IOException ex) {
//    		throw AppException.fileStorageException("Could not store file " + mFileName + ". Please try again!", ex);
    		return new Response(false, "Failed to Store File!");
    	}
    }

    @Override
    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName.toLowerCase()).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw AppException.fileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw AppException.fileNotFoundException("File not found " + fileName, ex);
        }
    }
    @Override
    public Resource loadFileAsResourceWithSubDir(String fileName, String subDir) {
    	try {
    		
    		Path fileDir = Paths.get(this.fileStorageLocation+"/"+subDir.toLowerCase()).toAbsolutePath().normalize();
    		
    		Path filePath = fileDir.resolve(fileName.toLowerCase()).normalize();
    		Resource resource = new UrlResource(filePath.toUri());
    		if(resource.exists()) {
    			return resource;
    		} else {
    			throw AppException.fileNotFoundException("File not found " + fileName);
    		}
    	} catch (MalformedURLException ex) {
    		throw AppException.fileNotFoundException("File not found " + fileName, ex);
    	}
    }

    
    
    
    //========================================================
	@Override
	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.fileStorageLocation, 1)
				.filter(path -> !path.equals(this.fileStorageLocation))
				.map(this.fileStorageLocation::relativize);
		} catch (IOException e) {
			throw AppException.fileStorageException("Failed to read stored files", e);
		}
	}

	@Override
	public Path load(String filename) {
		return this.fileStorageLocation.resolve(filename);
	}
	
	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(this.fileStorageLocation.toFile());
	}

	@Override
	public void init() {
		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (IOException e) {
			throw AppException.fileStorageException("Could not initialize storage", e);
		}
	}
	
	
	private boolean isEmpty(String str) {
		if (str == null)
			return true;
		if (str.trim().length() == 0)
			return true;
		return false;
	}
}
