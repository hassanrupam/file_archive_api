package com.swiftit.himsApp.config;

import java.util.HashMap;
import java.util.Map;

public class Response {
	
	private boolean success = true;
	private String message;
	private Map<String, Object> map;

	
    public Response(boolean success, String message) {
    	
    	this.success 		= success;
    	this.message 		= message;
    }
    public Response(boolean success, String message, String fileName, 
    		String downloadUrl, String fileType, long fileSize) {
    	
    	this.success 		= success;
    	this.message 		= downloadUrl;
    	
    	map = new HashMap<String, Object>();
    	map.put("success", 		success);
    	map.put("message", 		message);
    	map.put("fileName", 	fileName);
    	map.put("downloadUrl", 	downloadUrl);
    	map.put("fileType", 	fileType);
    	map.put("fileSize", 	fileSize);
    }
    
	public boolean isSuccess() {
		return success;
	}	
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Map<String, Object> getMap() {
		return map;
	}
	public void setMap(Map<String, Object> map) {
		this.map = map;
	}
	
	
	
}
