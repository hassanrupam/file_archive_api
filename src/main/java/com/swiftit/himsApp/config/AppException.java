package com.swiftit.himsApp.config;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class AppException {
	
	public static Exception saveExcepton(String message) {
        return new Exception(message!=null?message:"Failed to Save Data!");
    }
	
	public static RuntimeException fileStorageException(String message) {
		return new RuntimeException(message);
    }

    public static RuntimeException fileStorageException(String message, Throwable cause) {
    	return new RuntimeException(message, cause);
    }
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public static RuntimeException fileNotFoundException(String message) {
    	return new RuntimeException(message);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    public static RuntimeException fileNotFoundException(String message, Throwable cause) {
    	return new RuntimeException(message, cause);
    }

}
