package com.swiftit.himsApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.swiftit.himsApp.config.StorageProperties;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class HimsArchiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(HimsArchiveApplication.class, args);
	}

}
