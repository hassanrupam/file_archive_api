package com.swiftit.himsApp.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.swiftit.himsApp.config.Response;
import com.swiftit.himsApp.service.FileStorageService;

@RestController
public class UploadController {

	private static final Logger logger = LoggerFactory.getLogger(UploadController.class);
	
	private static final String URL_DOWNLOAD_FILE 			= "/download-file";
	private static final String URL_UPLOAD_FILE 			= "/upload-file";
	private static final String URL_UPLOAD_Multiple_FILE 	= "/upload-multiple-files";

    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping(URL_UPLOAD_FILE)
    public Response uploadFile(@RequestParam("file") MultipartFile file, 
    		@RequestParam(value = "name", required = false) String name, 
    		@RequestParam(value = "subDir", required = false) String subDir) {
    	
    	Response fileRes = fileStorageService.storeFile(file, name, subDir);
    	
    	if(fileRes.isSuccess()) {
    		fileRes.setMessage(URL_DOWNLOAD_FILE+"/"+fileRes.getMessage());
    		fileRes.getMap().put("downloadUrl", fileRes.getMessage());
    	}
//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/").path(fileName).toUriString();
    	return fileRes;
    }

    @PostMapping(URL_UPLOAD_Multiple_FILE)
    public List<Response> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files, 
    		@RequestParam(value = "name", required = false) String name, 
    		@RequestParam(value = "subDir", required = false) String subDir) {
    	
        return Arrays.asList(files).stream().map(file -> uploadFile(file, name, subDir)).collect(Collectors.toList());
    }

    @GetMapping(URL_DOWNLOAD_FILE+"/{filename:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String filename, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(filename);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
    
    @GetMapping(URL_DOWNLOAD_FILE+"/{subDir}/{filename:.+}")
    public ResponseEntity<Resource> downloadFileWithSubDir(@PathVariable String subDir, 
    		@PathVariable String filename, HttpServletRequest request) {
    	
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResourceWithSubDir(filename, subDir);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
    
}
