package com.swiftit.himsApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/testing")
public class TestController {
	
	@GetMapping("/text")
	public String update(@RequestBody(required = false) String reqObj) {
		return "Test Api Success";
	}

}
